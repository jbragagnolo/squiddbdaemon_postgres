create user squid;
create database squid_log owner squid;
\connect squid_log

CREATE TABLE access_log (
    id serial,
    time_since_epoch numeric(15,3),
    date_day date,
    date_time time without time zone,
    response_time integer,
    client_src_ip_addr character(30),
    squid_request_status character varying(50),
    http_status_code character varying(10),
    reply_size integer,
    request_method character varying(20),
    request_url character varying(500),
    username character varying(40),
    squid_hier_status character varying(20),
    server_ip_addr character(30),
    mime_type character varying(50)
);

ALTER TABLE ONLY access_log ADD CONSTRAINT access_log_pkey PRIMARY KEY (id);
ALTER TABLE public.access_log OWNER TO squid;

CREATE or replace FUNCTION fun_extract_date_bi() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

  NEW.date_day  := TO_CHAR(TO_TIMESTAMP(NEW.time_since_epoch), 'YYYY-MM-DD');
  NEW.date_time := TO_CHAR(TO_TIMESTAMP(NEW.time_since_epoch), 'HH24:MI:SS');
  RETURN NEW;
END;
$$;


ALTER FUNCTION public.fun_extract_date_bi() OWNER TO squid;

CREATE TRIGGER extract_date_bi BEFORE INSERT ON access_log FOR EACH ROW EXECUTE PROCEDURE fun_extract_date_bi();
