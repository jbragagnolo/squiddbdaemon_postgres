#!/bin/bash
pidFile='/var/run/squidDbDaemon.pid'
if [ "$1" == "stop" ]
then
	pkill -P `cat $pidFile`
#	pkill -P `ps ax | grep squidDbDaemon | egrep -vw '(grep|vi)' | awk '{print $1}'`
	exit 0
fi

if [ "$1" == "start" ]
then
	/usr/local/atua/squidDbDaemon.sh 2>>/var/log/squid3/squidDbDaemon.err 1>>/var/log/squid3/squidDbDaemon.out &
        exit 0
fi

if [ $# -eq 0 ]
then
sleep 30
echo "qtd=$#"
pid=$$
echo $pid > $pidFile
tail -n 0 -f /var/log/squid3/squid_mysql.log | awk '{
	insert="INSERT INTO access_log (time_since_epoch,response_time,client_src_ip_addr,squid_request_status,http_status_code,reply_size,request_method,request_url,username,squid_hier_status,server_ip_addr,mime_type) values (\x27"; 
	virgula="\x27,\x27";
	fechaInsert="\x27);";
	printf insert; 
	print $1,virgula,$2,virgula,$3,virgula,$4,virgula,$5,virgula,$6,virgula,$7,virgula,$8,virgula,$9,virgula,$10,virgula,$11,virgula,$12,fechaInsert;
	}' | mysql -usquid -pXsPqn squid_log 
fi
